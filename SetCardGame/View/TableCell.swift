//
//  TableCell.swift
//  SetCardGame
//
//  Created by Sebastian Stefaniak on 18.07.2018.
//  Copyright © 2018 Sebastian Stefaniak. All rights reserved.
//

import Foundation
import UIKit
class TableCell: UICollectionViewCell {
    
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var imageCover: UIImageView!
    
}
