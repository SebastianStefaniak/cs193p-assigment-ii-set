//
//  ViewController.swift
//  SetCardGame
//
//  Created by Sebastian Stefaniak on 17.07.2018.
//  Copyright © 2018 Sebastian Stefaniak. All rights reserved.
//

import UIKit

class GameCardViewController: UIViewController {
    
    private var game = SetGame()
    private var cardPlacesOnTable: [Int: CardPlace] = [:]
    
    @IBOutlet weak var score: UILabel!
    
    @IBOutlet weak var taps: UILabel!
    
    @IBOutlet weak var table: UICollectionView!
    
    @IBOutlet private weak var cardsDealButton: UIButton!
    
    @IBOutlet private weak var newGameButton: UIButton!
    
    @IBAction func startNewGame(_ sender: UIButton) {
        startNewGame()
    }
    
    @IBAction func putCardsOnTable(_ sender: UIButton) {
        game.dealAdditionalCards()
        table.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startNewGame()
    }
    
    private func startNewGame() {
        game.startNewGame()
        table.reloadData()
    }
    
    private func reloadDataFromModel() {
        score.text = game.scoreValue
        taps.text = game.tapsValue
        if game.cardsFromDeck.count == 0 || game.cardsOnTable.count == game.maxNumberOfCardsOnTable {
            cardsDealButton.isHidden = true
        } else {
            cardsDealButton.isHidden = false
        }
    }
    
    private func setCellView(_ cell: TableCell, at index: Int) {
        if let cardPlace = cardPlacesOnTable[index] {
            if let card = cardPlace.card, let gameCard = game.gameCards.filter({$0.identifier == card.identifier && $0.isOnTable}).first {
                cardPlacesOnTable[index]?.card = gameCard
                setApperanceOfCard(cell, card: gameCard)
            } else if let gameCard = game.getRandomCardFromDeck() {
                cardPlacesOnTable[index]?.card = gameCard
                setApperanceOfCard(cell, card: gameCard)
            } else {
                cardPlacesOnTable[index]?.card = nil
                cell.isHidden = true
            }
        } else if let gameCard = game.getRandomCardFromDeck() {
            let cardPlace = CardPlace(card: gameCard)
            cardPlacesOnTable[index] = cardPlace
            setApperanceOfCard(cell, card: gameCard)
        }
        reloadDataFromModel()
    }
    
    private func setApperanceOfCard(_ cell: TableCell, card: Card) {
        cell.isHidden = false
        setColorOfCardSymbols(cell, cardModel: card)
        setSymbolOfCard(cell, cardModel: card)
        setBackgroundOfMatchedCard(cell, cardModel: card)
    }
    
    private func setColorOfCardSymbols(_ cell: TableCell, cardModel card: Card) {
        switch card.color {
        case .firstColor:
            cell.cardLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        case .secondColor:
            cell.cardLabel.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        case .thirdColor:
            cell.cardLabel.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
    }
    
    private func setSymbolOfCard(_ cell: TableCell, cardModel card: Card) {
        var symbol = ""
        switch card.symbol {
        case .firstSymbol:
            symbol = "△"
        case .secondSymbol:
            symbol = "◻︎"
        case .thirdSymbol:
            symbol = "○"
        }
        
        switch card.cadSymbolCount {
        case .one:
            break
        case .two:
            symbol = symbol + "\n" + symbol
        case .three:
            symbol = symbol + "\n" + symbol + "\n" + symbol
        }
        
        switch card.shading {
        case .firstShading:
            let attributes: [NSAttributedStringKey: Any] = [
                NSAttributedStringKey.strokeColor: cell.cardLabel.textColor,
                NSAttributedStringKey.foregroundColor: cell.cardLabel.textColor,
                NSAttributedStringKey.strokeWidth: 0, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 30.0)
            ]
            cell.cardLabel.attributedText = NSAttributedString(string: symbol, attributes: attributes)
            cell.imageCover.isHidden = true
        case .secondShading:
            let strokeTextAttributes: [NSAttributedStringKey: Any] = [
                NSAttributedStringKey.strokeColor: cell.cardLabel.textColor,
                NSAttributedStringKey.foregroundColor: cell.cardLabel.textColor,
                NSAttributedStringKey.strokeWidth: -45, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)
            ]
            cell.cardLabel.attributedText = NSAttributedString(string: symbol, attributes: strokeTextAttributes)
            cell.imageCover.isHidden = true
        case .thirdShading:
            let attributes: [NSAttributedStringKey: Any] = [
                NSAttributedStringKey.strokeColor: cell.cardLabel.textColor,
                NSAttributedStringKey.foregroundColor: cell.cardLabel.textColor,
                NSAttributedStringKey.strokeWidth: -45, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)
            ]
            cell.cardLabel.attributedText = NSAttributedString(string: symbol, attributes: attributes)
            cell.imageCover.isHidden = false
        }
    }
    
    private func setBackgroundOfMatchedCard(_ cell: TableCell, cardModel card: Card) {
        if card.isMatched, card.isMatchedCorrectly == nil {
            cell.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        } else if card.isMatchedCorrectly != nil, card.isMatchedCorrectly! {
            cell.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        } else if card.isMatchedCorrectly != nil, !card.isMatchedCorrectly! {
            cell.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        } else if !card.isInDeck, !card.isOnTable {
            cell.isHidden = true
        } else {
            cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    private func choseCard(at index: Int) {
        guard let cardPlace = cardPlacesOnTable[index] else {
            return
        }
        guard let card = cardPlace.card else {
            return
        }
        game.choseCard(identifier: card.identifier)
        table.reloadData()
    }
}

// MARK: - UICollectionViewDelegate
extension GameCardViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return game.minNumberOfCardsOnTable
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TableCell", for: indexPath) as? TableCell else {
            fatalError()
        }
        setCellView(cell, at: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        choseCard(at: indexPath.row)
    }
}

// MARK: - UICollectionViewDataSource
extension GameCardViewController: UICollectionViewDataSource {
    
}

