//
//  SetGame.swift
//  SetCardGame
//
//  Created by Sebastian Stefaniak on 17.07.2018.
//  Copyright © 2018 Sebastian Stefaniak. All rights reserved.
//

import Foundation

class SetGame {
    let maxNumberOfCardsOnTable = 24
    var minNumberOfCardsOnTable: Int {
        return 12 + additionalCards
    }
    var gameCards: [Card] = []
    var scoreValue: String {
        return "Score: \(score)"
    }
    var tapsValue: String {
        return "Taps: \(taps)"
    }
    var cardsOnTable: [Card] {
        return gameCards.filter({$0.isOnTable})
    }
    var cardsFromDeck: [Card] {
        return gameCards.filter({$0.isInDeck && !$0.isOnTable})
    }
    private var matchedCards: [Card] {
        return gameCards.filter({$0.isOnTable && $0.isMatched})
    }
    private let numberOfMatchedCards = 3
    private var score = 0, taps = 0, additionalCards = 0
    private var correctlyMatchedCard: [Card] {
        return gameCards.filter({$0.isOnTable && $0.isMatched && $0.isMatchedCorrectly != nil && $0.isMatchedCorrectly!})
    }
    
    func startNewGame() {
        gameCards.removeAll()
        score = 0
        taps = 0
        additionalCards = 0
        for shading in CardShadings.allValues {
            for color in CardColors.allValues {
                for symbol in CardSymbols.allValues {
                    for numberOfSymols in CardSymbolCount.allValues {
                        let card = Card(shading: shading, color: color, symbol: symbol, numberOfSymbol: numberOfSymols)
                        gameCards.append(card)
                    }
                }
            }
        }
    }
    
    func choseCard(identifier: Int) {
        if matchedCards.count < numberOfMatchedCards {
            taps += 1
            guard let card = gameCards.filter({$0.identifier == identifier}).first else {
                return
            }
            if card.isMatched {
                deselectCard(identifier: card.identifier)
                score -= 1
            } else {
                selectCard(identifier: card.identifier)
                if matchedCards.count == numberOfMatchedCards {
                    compareChosenCards()
                }
            }
        } else {
            removeOrDeselectCard()
        }
    }
    
    func dealAdditionalCards() {
        if cardsFromDeck.count >= numberOfMatchedCards, cardsOnTable.count < maxNumberOfCardsOnTable {
            score -= 1
            additionalCards += numberOfMatchedCards
        }
    }
    
    func getRandomCardFromDeck() -> Card? {
        let randomIndexOfCardInDeck = cardsFromDeck.count.arc4random
        if cardsFromDeck.count > 0 {
            let cardFromDeck = cardsFromDeck[randomIndexOfCardInDeck]
            putCardOnTable(identifier: cardFromDeck.identifier)
            return cardFromDeck
        }
        return nil
    }
    
    private func removeOrDeselectCard() {
        for matchedCard in matchedCards {
            if correctlyMatchedCard.count > 0 {
                score += 1
                removeCardFromTable(identifier: matchedCard.identifier)
            } else {
                score -= 1
                deselectCard(identifier: matchedCard.identifier)
            }
        }
    }
    
    private func compareChosenCards() {
        assert(matchedCards.count == 3 || numberOfMatchedCards == 3, "SetGame.compareChosenCards - chosecCards count value must be equal to 3.")
        let firstCard = gameCards[gameCards.index(of: matchedCards[0])!]
        let secondCard = gameCards[gameCards.index(of: matchedCards[1])!]
        let thirdCard = gameCards[gameCards.index(of: matchedCards[2])!]
        let resultOfCompareColors = compareValues(firstValue: firstCard.color.rawValue, secondValue: secondCard.color.rawValue, thirdValue: thirdCard.color.rawValue)
        let resultOfCompareNumberOfSymbol = compareValues(firstValue: firstCard.cadSymbolCount.rawValue, secondValue: secondCard.cadSymbolCount.rawValue, thirdValue: thirdCard.cadSymbolCount.rawValue)
        let resultOfCompareShading = compareValues(firstValue: firstCard.shading.rawValue, secondValue: secondCard.shading.rawValue, thirdValue: thirdCard.shading.rawValue)
        let resultOfCompareSymbols = compareValues(firstValue: firstCard.symbol.rawValue, secondValue: secondCard.symbol.rawValue, thirdValue: thirdCard.symbol.rawValue)
        print("result of compare \(resultOfCompareColors) \(resultOfCompareShading) \(resultOfCompareSymbols) \(resultOfCompareNumberOfSymbol)")
        for card in matchedCards {
            if resultOfCompareColors, resultOfCompareNumberOfSymbol, resultOfCompareShading, resultOfCompareSymbols {
                setCardAsCorrectChoice(identifier: card.identifier)
            }else{
                setCardAsFailureChoice(identifier: card.identifier)
            }
        }
    }
    
    private func compareValues(firstValue: Int, secondValue: Int, thirdValue: Int) -> Bool {
        return (firstValue == secondValue && secondValue == thirdValue) || (firstValue != secondValue && secondValue != thirdValue && firstValue != thirdValue)
    }
    
    private func selectCard(identifier: Int) {
        if var card = gameCards.filter({$0.identifier == identifier}).first {
            card.isMatched = true
            card.isOnTable = true
            card.isInDeck = false
            card.isMatchedCorrectly = nil
            changeCardOnDeck(card: card)
        }
    }
    
    private func deselectCard(identifier: Int) {
        if var card = gameCards.filter({$0.identifier == identifier}).first {
            card.isMatched = false
            card.isOnTable = true
            card.isInDeck = false
            card.isMatchedCorrectly = nil
            changeCardOnDeck(card: card)
        }
    }
    
    private func setCardAsCorrectChoice(identifier: Int) {
        if var card = gameCards.filter({$0.identifier == identifier}).first {
            card.isMatched = true
            card.isOnTable = true
            card.isInDeck = false
            card.isMatchedCorrectly = true
            changeCardOnDeck(card: card)
        }
    }
    
    private func setCardAsFailureChoice(identifier: Int) {
        if var card = gameCards.filter({$0.identifier == identifier}).first {
            card.isMatched = true
            card.isOnTable = true
            card.isInDeck = false
            card.isMatchedCorrectly = false
            changeCardOnDeck(card: card)
        }
    }
    
    private func putCardOnTable(identifier: Int) {
        if var card = gameCards.filter({$0.identifier == identifier}).first {
            card.isMatched = false
            card.isOnTable = true
            card.isInDeck = false
            card.isMatchedCorrectly = nil
            changeCardOnDeck(card: card)
        }
    }
    
    private func removeCardFromTable(identifier: Int) {
        if var card = gameCards.filter({$0.identifier == identifier}).first {
            card.isMatched = false
            card.isOnTable = false
            card.isInDeck = false
            card.isMatchedCorrectly = nil
            changeCardOnDeck(card: card)
        }
    }
    
    private func changeCardOnDeck(card: Card) {
        if let cardIndex = gameCards.index(of: card) {
            gameCards[cardIndex] = card
        }
    }
}

// MARK: - Int
extension Int {
    var arc4random: Int {
        return Int(arc4random_uniform(UInt32(self)))
    }
}
