//
//  SetCards.swift
//  SetCardGame
//
//  Created by Sebastian Stefaniak on 17.07.2018.
//  Copyright © 2018 Sebastian Stefaniak. All rights reserved.
//

import Foundation
import UIKit

struct Card {
    var isMatched = false, isOnTable = false, isInDeck = true
    var isMatchedCorrectly: Bool?
    var identifier: Int
    var shading: CardShadings
    var color: CardColors
    var symbol: CardSymbols
    var cadSymbolCount: CardSymbolCount
    
    private static var identifierFactory = 0
    
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    init(shading: CardShadings, color: CardColors, symbol: CardSymbols, numberOfSymbol: CardSymbolCount) {
        self.shading = shading
        self.color = color
        self.symbol = symbol
        self.cadSymbolCount = numberOfSymbol
        identifier = Card.getUniqueIdentifier()
    }
}

extension Card: Equatable {
    
    var hashValue: Int {
        return identifier
    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}

enum CardColors: Int {
    case firstColor
    case secondColor
    case thirdColor
    static let allValues = [firstColor, secondColor, thirdColor]
}

enum CardSymbols: Int {
    case firstSymbol
    case secondSymbol
    case thirdSymbol
    static let allValues = [firstSymbol, secondSymbol, thirdSymbol]
}

enum CardShadings: Int {
    case firstShading
    case secondShading
    case thirdShading
    static let allValues = [firstShading, secondShading, thirdShading]
}

enum CardSymbolCount: Int {
    case one
    case two
    case three
    static let allValues = [one, two, three]
}

